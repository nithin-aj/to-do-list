/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.todolist.ui.service.impl;
import in.ac.gpckasaragod.todolist.service.model.ui.MainTask;
import in.ac.gpckasaragod.todolist.ui.service.MainTaskService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
  public class MainTaskImpl extends ConnectionServiceImpl implements MainTaskService{

    @Override
    public String saveMainTask(String taskName, Date startDate, Date endDate, String status) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO MAINTASK(TASKNAME,STARTDATE,ENDDATE,STATUS) VALUES" + "('"+taskName+"','"+startDate+"','"+endDate+"')";
            System.err.println("Query:"+query);
            int  insert = statement.executeUpdate(query);
            if(insert !=1){
                return "save failed";
            }else{
                return "saved successfully";
            }
       }catch (SQLException ex){
          Logger.getLogger(MainTaskImpl.class.getName()).log(Level.SEVERE,null,ex);
          return "save failed";
      } 
    }

    @Override
    public MainTask readMainTask(Integer id) {
        MainTask mainTask= null;
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
          String query = "SELECT * FROM MAINTASK WHERE ID="+id;
           System.err.println("Query:"+query);
           ResultSet resultSet = statement.executeQuery(query);
          while(resultSet.next()){
            String taskName = resultSet.getString("TASKNAME");
            String startDate = resultSet.getString("STARTDATE");
            String endDate = resultSet.getString("ENDDATE");
            String status = resultSet.getString("STATUS");
            mainTask = new MainTask(taskName,startDate,endDate,status);
          }
        } catch (SQLException ex) {
            Logger.getLogger(MainTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mainTask;
    }

    @Override
    public List<MainTask> getAllMainTask() {
        List<MainTask> mainTasks = new ArrayList<MainTask>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
         String query = "SELECT * FROM MAINTASK";
         System.err.println("Query:"+query);
         ResultSet resultSet = statement.executeQuery(query);
          while(resultSet.next()){
               String taskName = resultSet.getString("TASKNAME");
               String startDate = resultSet.getString("STARTDATE");
               String endDate = resultSet.getString("ENDDATE");
               String status = resultSet.getString("STATUS");
               MainTask mainTask = new MainTask(taskName,startDate,endDate,status);
               mainTasks.add(mainTask);
           }
            
        } catch (SQLException ex) {
            Logger.getLogger(MainTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mainTasks;
    }

    @Override
    public String updateMainTask(Integer id, String taskName, Date startDate, Date endDate, String status) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE MAINTASK SET TASKNAME ='"+taskName+"',STARTDATE ='"+startDate+"',ENDDATE ='"+endDate+"',STATUS ='"+status+"' WHERE ID="+id;             
            System.err.println("Query:"+query);
             int update = statement.executeUpdate(query);
             if(update !=1)
             return "Update Failed";
             else
                 return "Update successfully";
        } catch (SQLException ex) {
            Logger.getLogger(MainTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
       
    
  

  
    
 
    

    @Override
    public String deleteMainTask(Integer id) {
      try {
          //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
          Connection connection = getConnection();
          String query = "DELETE FROM MAINTASK WHERE ID =?";
           PreparedStatement statement = connection.prepareStatement(query);
           statement.setInt(1, id);
           int delete = statement.executeUpdate();
           if(delete !=1)
               return "Delete failed";
           else
               return "Delete successfully";
      } catch (SQLException ex) {
          Logger.getLogger(MainTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
        return null;
    }
  }
      
     
          