/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.todolist.ui.service;

import in.ac.gpckasaragod.todolist.service.model.ui.MainTask;
import java.util.Date;
import java.util.List;

/**
 *
 * @author student
 */

public interface MainTaskService {
    public String saveMainTask(String taskName,Date startDate,Date endDate,String status);
    public MainTask readMainTask(Integer id);
    public List<MainTask>getAllMainTask();
    public String updateMainTask(Integer id,String taskName,Date startDate,Date endDate,String status);
    public String deleteMainTask(Integer id);
}
    
    

