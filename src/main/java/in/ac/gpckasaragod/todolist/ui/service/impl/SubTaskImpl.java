/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.todolist.ui.service.impl;

import in.ac.gpckasaragod.todolist.service.model.ui.MainTask;
import in.ac.gpckasaragod.todolist.service.model.ui.SubTask;
import in.ac.gpckasaragod.todolist.ui.service.MainTaskService;
import in.ac.gpckasaragod.todolist.ui.service.SubTaskService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class SubTaskImpl extends ConnectionServiceImpl implements SubTaskService{

    @Override
    public String saveSubTask(String taskName, Integer mainTaskId, Date startDate, Date endDate, String status) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
                
            
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO SUBTASK(TASKNAME,STARTDATE,ENDDATE,STATUS) VALUES" + "('"+taskName+"','"+mainTaskId+"','"+startDate+"','"+endDate+"')";
            int  insert = statement.executeUpdate(query);
            if(insert !=1){
                return "save failed";
            }else{
                return "saved successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "save failed";
        }
}

    @Override
    public MainTask readSubTask(Integer id) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            Statement statement = connection.createStatement();
          String query = "SELECT * FROM SUBTASK WHERE ID="+id;
           System.err.println("Query:"+query);
           ResultSet resultSet = statement.executeQuery(query);
          while(resultSet.next()){
            String taskName = resultSet.getString("TASKNAME");
            String startDate = resultSet.getString("STARTDATE");
            String endDate = resultSet.getString("ENDDATE");
            String status = resultSet.getString("STATUS");
            subTask = new MainTask(taskName,mainTaskId,startDate,endDate,status);
          }
        } catch (SQLException ex) {
            Logger.getLogger(SubTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
        
    
    @Override
    public List<SubTask> getAllSubTask() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        List<SubTask> subTasks = new ArrayList<SubTask>(); 
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
         String query = "SELECT * FROM SUBTASK";
         System.err.println("Query:"+query);
         ResultSet resultSet = statement.executeQuery(query);
          while(resultSet.next()){
               String taskName = resultSet.getString("TASKNAME");
               String mainTaskId =resultSet.getString("MAINTASKID");
               String startDate = resultSet.getString("STARTDATE");
               String endDate = resultSet.getString("ENDDATE");
               String status = resultSet.getString("STATUS");
               SubTask subTask = new SubTask(taskName,mainTaskId,startDate,endDate,status);
               subTasks.add(subTask);
           }
            
        } catch (SQLException ex) {
            Logger.getLogger(SubTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return subTasks;
    }
           

    @Override
    public String updateSubTask(Integer id, String taskName, Integer mainTaskId, Date startDate, Date endDate, String status) {
        try{
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
            String query = "UPDATE SUBTASK SET TASKNAME ='"+taskName+"',MAINTASKID='"+mainTaskId+"',STARTDATE ='"+startDate+"',ENDDATE ='"+endDate+"',STATUS ='"+status+"' WHERE ID="+id;             
            System.err.println("Query:"+query);
             int update = statement.executeUpdate(query);
             if(update !=1)
             return "Update Failed";
             else
                 return "Update successfully";
        } catch (SQLException ex) {
            Logger.getLogger(SubTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
       
        
        

    @Override
    public String deleteSubTask(Integer id) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            String query = "DELETE FROM SUBTASK WHERE ID =?";
           PreparedStatement statement = connection.prepareStatement(query);
           statement.setInt(1, id);
           int delete = statement.executeUpdate();
           if(delete !=1)
               return "Delete failed";
           else
               return "Delete successfully";
      } catch (SQLException ex) {
          Logger.getLogger(SubTaskImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
        return null;
    }
}
