/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.todolist.ui.service;

import in.ac.gpckasaragod.todolist.service.model.ui.MainTask;
import in.ac.gpckasaragod.todolist.service.model.ui.SubTask;
import java.util.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface SubTaskService {
    public String saveSubTask(String taskName,Integer mainTaskId,Date startDate,Date endDate,String status);
    public MainTask readSubTask(Integer id);
    public List<SubTask>getAllSubTask();
    public String updateSubTask(Integer id,String taskName,Integer mainTaskId,Date startDate,Date endDate,String status);
    public String deleteSubTask(Integer id);
    
}
