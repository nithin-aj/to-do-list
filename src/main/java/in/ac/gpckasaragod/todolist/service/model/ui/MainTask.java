/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.todolist.service.model.ui;

import java.sql.Date;

/**
 *
 * @author student
 */
public class MainTask {
  private Integer Id;
    private String TaskName;
    private Date StartDate;
    private Date EndDate;
    private String Status;

    public MainTask(String taskName, String startDate, String endDate, String status) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getTaskName() {
        return TaskName;
    }

    public void setTaskName(String TaskName) {
        this.TaskName = TaskName;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date StartDate) {
        this.StartDate = StartDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date EndDate) {
        this.EndDate = EndDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public MainTask(Integer Id, String TaskName, Date StartDate, Date EndDate, String Status) {
        this.Id = Id;
        this.TaskName = TaskName;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.Status = Status;
    }
    

   
}
