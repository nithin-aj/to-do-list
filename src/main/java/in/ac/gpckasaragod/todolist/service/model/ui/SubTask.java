/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.todolist.service.model.ui;

import java.sql.Date;

/**
 *
 * @author student
 */
public class SubTask {
    private Integer Id;
    private String TaskName;
    private Integer MainTaskId;
    private Date StartDate;
    private Date EndDate;
    private String Status;

    public SubTask(Integer Id, String TaskName, Integer MainTaskId, Date StartDate, Date EndDate, String Status) {
        this.Id = Id;
        this.TaskName = TaskName;
        this.MainTaskId = MainTaskId;
        this.StartDate = StartDate;
        this.EndDate = EndDate;
        this.Status = Status;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getTaskName() {
        return TaskName;
    }

    public void setTaskName(String TaskName) {
        this.TaskName = TaskName;
    }

    public Integer getMainTaskId() {
        return MainTaskId;
    }

    public void setMainTaskId(Integer MainTaskId) {
        this.MainTaskId = MainTaskId;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date StartDate) {
        this.StartDate = StartDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date EndDate) {
        this.EndDate = EndDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    
}
