/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.todolist.service.model.ui;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class UserService {
    private Integer id;
    private String UserName;
    private String EmailId;
    
public UserService(Integer id, String UserName, String EmailId) {
        this.id = id;
        this.UserName = UserName;
        this.EmailId = EmailId;
    }
    private static final Logger LOG = Logger.getLogger(UserService.class.getName());

    public UserService(String id, Integer userName, String id0, String emailid) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

   // public UserService(String id, Integer userName, String id0, String emailid) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    //}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String EmailId) {
        this.EmailId = EmailId;
    }
}

